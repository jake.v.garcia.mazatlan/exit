async function buscarCoctel() {
    const nombreCoctel = document.getElementById('txtNombre').value.trim(); // Eliminamos espacios en blanco
    const resultadoDiv = document.getElementById('resultado');

    if (nombreCoctel === '') {
        resultadoDiv.textContent = 'La búsqueda está vacía. Por favor, ingrese un nombre de cóctel.';
        return; // Salimos de la función si la búsqueda está vacía
    }

    try {
        const response = await axios.get(`https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${nombreCoctel}`);
        const cocteles = response.data.drinks;

        resultadoDiv.innerHTML = ''; // Limpiamos el contenido antes de mostrar los resultados

        if (cocteles) {
            const coctel = cocteles[0]; // Tomamos solo la primera bebida si hay resultados
            console.log(cocteles)
            const coctelless = cocteles.find(c=> c.strDrink === nombreCoctel)
            if(coctelless){
                const coctelInfo = document.createElement('div');
                coctelInfo.classList.add('coctel-info');
    
                const categoria = document.createElement('p');
                categoria.textContent = `Categoría: ${coctel.strCategory}`;
                coctelInfo.appendChild(categoria);
    
                const instrucciones = document.createElement('p');
                instrucciones.textContent = `Instrucciones: ${coctel.strInstructions}`;
                coctelInfo.appendChild(instrucciones);
    
                const img = document.createElement('img');
                img.src = coctel.strDrinkThumb;
                img.classList.add('coctel-img');
                coctelInfo.appendChild(img);
    
                const limpiarBtn = document.createElement('button');
                limpiarBtn.textContent = 'Limpiar';
                limpiarBtn.onclick = limpiarResultados;
                coctelInfo.appendChild(limpiarBtn);
    
                resultadoDiv.appendChild(coctelInfo);
            }else{
                const p = document.createElement('p')
                p.textContent = `no se encontro el coctel con el nombre: ${nombreCoctel}`;
                resultadoDiv.appendChild(p)
            }

            
        } else {
            resultadoDiv.textContent = 'No se encontraron cócteles con ese nombre.';
        }
    } catch (error) {
        console.error('Error al buscar el coctel:', error);
        resultadoDiv.textContent = 'Ocurrió un error al buscar el cóctel. Por favor, inténtalo de nuevo más tarde.';
    }
}

function limpiarResultados() {
    document.getElementById('resultado').innerHTML = '';
}
